Bob Rein
lab 8
Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?

     ANSWER: The console log does not show some of the lines from the logger because in the loggeer.properties file the console level is set to INFO and the logger file level is set to ALL. The console will only display errors that are INFO or above and FINER is below INFO, so the FINER errors only show up in the logger.log and not the console.
1.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']

	ANSWER: That line is coming from the logger inside of JUnit and gets passed to our logger. More specifically it comes from the class org.junit.jupiter.engine.execution.ConditionEvaluator.
1.  What does Assertions.assertThrows do?
	
	ANSWER: Assertions.assertThrows checks to make sure the right exception was thrown and it will fail the test otherwise.

1.  See TimerException and there are 3 questions
   1.  What is serialVersionUID and why do we need it? (please read on Internet)
 
	ANSWER: The serialVersionUID allows us to verify that the sender and reciever of the serialized data have compatible classes in order to successfully deserialize the data.
   2.  Why do we need to override constructors?
 
    ANSWER: We override constructors so that the parent option can access the same parameters when they get passed back up. It is only kind of like overriding them because we are technically just making constructors for those classes.
   3.  Why we did not override other Exception methods?
 
     ANSWER:	We only needed to override the methods that we did. We did not need different functionality for all of the other methods in Exception.
1.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)
	ANSWER: The static block is run the first time the object is run. Basically what it does, is helps set up and initialize values.	
1.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)
	ANSWER: The gives people something easier to read about the code.The markdown file format allows for prettier styling and it is supported and parsed on BitBucket and all online repo hosting services like BitBucket or GitHub.
1.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)
	ANSWER: The reason it is failing is because it is geting a null pointer exception and it is expecting a TimerExecption. I fixed it by moving the line in timer.java that intiallized timeNow to be above the if statement but within the try. Now when finally runs we won't get a null pointer exception because timenow has been intialized but the Timer exception is still thrown back up
1.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)
	ANSWER: The TimerException was getting swallowed up inside the try catch block and it was throwing the null pointer exception full.
1.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 
	ANSWER: see image included
1.  Make a printScreen of your eclipse Maven test run, with console
	ANSWER: see image included
1.  What category of Exceptions is TimerException and what is NullPointerException
	ANSWER: The Timer Exception is a User Defined Exception and the NullPointerException is a Built in Runtime Exception.


1.  Push the updated/fixed source code to your own repository.